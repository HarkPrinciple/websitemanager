using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Linq;
using System.Net;
using System.IO;
using System;

using Hark.Cloud.Server;

namespace Hark.Cloud.Extension
{
    public class ServerExtension : IProcessServerSessionExtension
    {
        public override string CommandName
        {
            get
            {
                return "website";
            }
        }

        protected override string ClaimName
        {
            get
            {
                return CommandName + ".claim";
            }
        }

        protected override void InitializeProcess(ProcessStartInfo startInfo, Session session, XElement doc)
        {
            startInfo.WorkingDirectory = "website";
            startInfo.FileName = "php";
            startInfo.Arguments = "-S 0.0.0.0:" + int.Parse(Session.GetOrThrow(doc, "/start/port").Trim());
        }

        public override XElement Compute(
            Session session,
            IPEndPoint sender,
            XElement doc)
        {
            XElement xcmd = doc.XPathSelectElement("/*");
            if(xcmd == null)
                return new XElement(CommandName,
                    HelpElement.CreateHelpElement(new HelpElement[]
                    {
                        new HelpElement("start", desc : "Start the website on the specified port", format : "start/port={port}"),
                        new HelpElement("stop", desc : "Stop the website", format : "stop"),
                        new HelpElement("claim", desc : "Claim that you are the administrator", format : "claim")
                    })
                );

            string cmd = xcmd.Name.LocalName.Trim().ToLower();
            switch(cmd)
            {
                case "start":
                    return Start(session, doc);

                case "stop":
                    return Stop(session, doc);

                case "claim":
                    return Claim(session);
                
                default:
                    return Session.GetError(1, "Can't understand the command \"" + cmd + "\"");
            }
        }
    }
}