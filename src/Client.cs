using System.Collections.Generic;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Linq;
using System;

using Hark.Cloud.Client;
using HarkLib.Core;

namespace Hark.Cloud.Extension
{
    using Client = Hark.Cloud.Client.Client;
    
    public class ClientExtension : IClientSessionExtension
    {
        public IDictionary<string, string> Help
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    { "start <port>", "Start the website on the specified port" },
                    { "stop", "Stop the website" },
                    { "claim", "Claim that you are the administrator" }
                };
            }
        }

        public void LoadCommands(Client client, ConsoleManager.Arguments args)
        {
            args
            .ExtractCommand("start/{port}", (cas, das) =>
                client.Query("website/start/port=" + das["port"].Trim()))
            .ExtractCommand("stop", (cas, das) =>
                client.Query("website/stop"))
            .ExtractCommand("claim", (cas, das) =>
                client.Query("website/claim"));
        }
    }
}